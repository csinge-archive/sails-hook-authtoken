/**
 * Created by csinge on 25/10/15.
 */
var helper = function()
{

}

helper.prototype.getToken = function(req)
{
    var headerContent = req.headers["x-auth-header"];
    if(!headerContent)
    {
        return null;
    }
    var rawToken = headerContent.split(";");
    if(rawToken.length != 2)
    {
        return null;
    }
    return {
        user : rawToken[0],
        key : rawToken[1]
    }
}

module.exports = new helper()