/**
 * Created by csinge on 24/10/15.
 */
var uuid = require('node-uuid');
var moment = require("moment")
var Q = require("q")

var tokenIssuer = module.exports  = function(cache){
    this.cache = cache;
    this.prefix = "authtoken-"
}

tokenIssuer.prototype.issue = function(userId){
    var token = {
        user : userId,
        key : uuid.v4()
    }
    var hash = {};
    hash[token.key] = moment().toISOString()
    this.cache.set(this.prefix+userId,hash)

    return Q.fcall(function(){
        return token;
    })
}

tokenIssuer.prototype.revoke = function(token){
    if(!this.isValidToken(token))
    {
        return Q.fcall(function(){
            throw new Error("Invalid token")
        })
    }
    var hash = {};
    hash[token.key] = "revoked";
    this.cache.set(this.prefix+token.user,hash)
    return Q.fcall(function(){ return token })
}

tokenIssuer.prototype.isValidToken = function(token)
{
    return !!token && !!token.user && !!token.key
}

tokenIssuer.prototype.validate = function(token){

    if(!this.isValidToken(token))
    {
        return false;
    }

    return this.cache.get(this.prefix+token.user).then(function(foundUser){
        return foundUser && typeof(foundUser[token.key]) != "undefined" && foundUser[token.key] != "revoked"
    })


}


