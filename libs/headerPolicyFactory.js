/**
 * Created by csinge on 24/10/15.
 */
var tokenHeaderHelper = require("./tokenHeaderHelper")
module.exports = function(issuer)
{
    return function(req,res,next)
    {
        //console.log("Authenticating towards header")
        var token = tokenHeaderHelper.getToken(req)
        if(token)
        {
            issuer.validate(token).then(function(result){
                if(result)
                {
                    next()
                }
                else
                {
                    res.forbidden('Valid token require');
                }
            }).catch(function(err){
                next(err)
            })
        }
        else
        {
            res.forbidden('Valid token require')
        }


    }
}