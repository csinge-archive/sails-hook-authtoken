'use strict';

//var redis = require("redis")
var Q = require("q")
var tokenIssuer = require("./libs/tokenIssuer")
var headerPolicyFactory = require("./libs/headerPolicyFactory")
var tokenHeaderHelper = require("./libs/tokenHeaderHelper")

module.exports = function (app) {
    var issuer;
    var policy;
    return {

        initialize: function (cb) {
            app.on('hook:redis:loaded', function () {
                issuer = new tokenIssuer(app.hooks.redis)
                policy = headerPolicyFactory(issuer);
                return cb();

            });
        },
        policy : function(){
            return policy
        },


        validate : function(token){
            return issuer.validate(token)
        },

        routes: {
            before: {
                /* TODO: Need to add a user identifier */
                'all /*': function localize(req, res, next) {

                    req.getAuth = function () {
                        var token = tokenHeaderHelper.getToken(req);
                        return {
                            user: token ? token.user : null,
                            token: token,
                        }
                    };
                    next();
                }
            }
        },
        issue: function (userId) {
            return issuer.issue(userId)
        },
        revoke: function (token) {
            return issuer.revoke(token)
        }
    };
};
