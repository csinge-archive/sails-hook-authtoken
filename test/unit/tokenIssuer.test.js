/**
 * Created by csinge on 24/10/15.
 */


var tokenIssuer = require("../../libs/tokenIssuer")
var Q = require("q")
var expect = require("chai").expect;
var cacheHelper = require("../helper/cacheHelper")


describe("Token issuer",function(){
    var issuer;
    var userId = "1"
    beforeEach(function(){
        issuer = new tokenIssuer(cacheHelper())
    })

    it("Should Issue a token", function(done){
        issuer.issue(userId).then(function(token){
            expect(token.user).to.equal(userId);
            expect(token.key).to.be.ok;
            done();
        })
    })

    it("Should Validate token", function(done){
        issuer.issue(userId).then(function(token){
            return issuer.validate(token);
        }).then(function(result){
            expect(result).to.be.true;
            done();
        }).catch(function(err){
            done(err);
        })
    })

    it("Should Invalidate token", function(done){
        issuer.issue(userId).then(function(token){
            return issuer.validate({user:userId,key:"4"});
        }).then(function(result){
            expect(result).to.be.false;
            done();
        }).catch(function(err){
            console.log(err)
        })
    })

    it("Should revoke token", function(done){
        var issuedToken
        issuer.issue(userId).then(function(token){
            issuedToken = token;
            issuer.revoke(issuedToken);
        }).then(function(){
            return issuer.validate(issuedToken);
        }).then(function(results){
            expect(results).to.be.false;
            done();
        })
    })


})