/**
 * Created by csinge on 24/10/15.
 */

var tokenIssuer = require("../../libs/tokenIssuer")
var headerPolicyFactory = require("../../libs/headerPolicyFactory")
var Q = require("q")
var expect = require("chai").expect;

var cacheHelper = require("../helper/cacheHelper")


describe("Header policy",function(){

    var userId = "1"
    var request = { };
    var issuer;
    var policy;
    var response = {};
    beforeEach(function(){
        request.headers = {};
        issuer = new tokenIssuer(cacheHelper())
        policy = headerPolicyFactory(issuer);
    })



    it("Should fail on missing header", function(done){
        response.forbidden = function(msg)
        {
            expect(msg).to.be.ok;
            done()
        }
        policy(request, response, function(err){
            done(err || "Should fail here");
        })
    })

    it("Should pass through on with valid issued header", function(done){
        issuer.issue(userId).then(function(token){
            request.headers["x-auth-header"] = token.user+";"+token.key
            policy(request, {}, function(err){
                expect(err).to.not.be.ok
                done();
            })
        })



    })

    it("Should pass through on with valid issued header", function(done){
        response.forbidden = function(msg)
        {

            expect(msg).to.be.ok;
            done();
        }
        issuer.issue(userId).then(function(token){
            return issuer.revoke(token)
        }).then(function(token){
            request.headers["x-auth-header"] = token.user+";"+token.key
            policy(request, response, function(err){
                done(err || "Should fail here");
            })
        }).catch(function(err){
            console.log(err)
        })

    })




})