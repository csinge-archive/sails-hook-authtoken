'use strict';

var expect = require('chai').expect;
var lifecycle = require('../helper/lifecycle.helper');

describe('Integration :: ', function() {

  var server1, server2;

  before(function (done) {
    lifecycle.setup2Servers(function(err, result) {
      if (err) {
        return done(err);
      }
      server1 = result.server1;
      server2 = result.server2;
      done();
    })
  });

  after(function (done) {
    lifecycle.teardown(server1, function () {
      lifecycle.teardown(server2, done);
    })
  });

  it('two servers should be created', function () {
    expect(server1).to.be.ok;
    expect(server2).to.be.ok;
    expect(server1.hooks.authtoken).to.be.ok
    expect(server2.hooks.authtoken).to.be.ok
  });



});
