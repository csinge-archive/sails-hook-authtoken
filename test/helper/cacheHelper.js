var Q = require("q")

module.exports = function(){
    var value = null;
    this.set = function(key, obj)
    {
        if(!obj)
        {
            value = null;
        }
        value = obj;
    }
    this.get = function(key)
    {
        return Q.fcall(function(){
            return value;
        })
    }
    return this;
}